<?php

declare(strict_types=1);

namespace Smtm\L10n\Locale;

use Smtm\Base\Application\Service\ApplicationService\Factory\DbConfigurableEntityManagerNameServiceDelegator;
use Smtm\Base\Application\Service\ApplicationServicePluginManager;
use Smtm\L10n\Locale\Context\LocalePosix\Application\Service\LocalePosixService;
use Smtm\L10n\Locale\Factory\L10nLocaleConfigAwareDelegator;
use Psr\Container\ContainerInterface;

return [
    'delegators' => [
        ApplicationServicePluginManager::class => [
            function (
                ContainerInterface $container,
                $name,
                callable $callback,
                array $options = null
            ) {
                /** @var ApplicationServicePluginManager $applicationServicePluginManager */
                $applicationServicePluginManager = $callback();

                $applicationServicePluginManager->addDelegator(
                    LocalePosixService::class,
                    L10nLocaleConfigAwareDelegator::class
                );
                $applicationServicePluginManager->addDelegator(
                    LocalePosixService::class,
                    DbConfigurableEntityManagerNameServiceDelegator::class
                );

                return $applicationServicePluginManager;
            }
        ],
    ],
];
