<?php

declare(strict_types=1);

namespace Smtm\L10n\Locale;

return [
    'orm' => [
        'mapping' => [
            'paths' => [
                \Smtm\L10n\Locale\Context\LocalePosix\Domain\LocalePosix::class =>
                    __DIR__ . '/../src/Context/LocalePosix/Infrastructure/Doctrine/Orm/Mapping',
            ],
        ],
    ],
];
