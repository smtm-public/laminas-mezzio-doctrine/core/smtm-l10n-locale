<?php

declare(strict_types=1);

namespace Smtm\L10n\Locale;

use Smtm\Base\Infrastructure\Helper\EnvHelper;

if (file_exists(__DIR__ . '/../../../../.env.smtm.smtm-l10n-locale')) {
    $dotenv = \Dotenv\Dotenv::createMutable(__DIR__ . '/../../../../', '.env.smtm.smtm-l10n-locale');
    $dotenv->load();
}

return [
    'entityManagerName' => EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_L10N_LOCALE_DB_ENTITY_MANAGER_NAME'),
    'readerEntityManagerName' => EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_L10N_LOCALE_DB_READER_ENTITY_MANAGER_NAME'),
    'archivedAccessEntityManagerName' => EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_L10N_LOCALE_DB_ARCHIVED_ACCESS_ENTITY_MANAGER_NAME'),
    'archivedAccessReaderEntityManagerName' => EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_L10N_LOCALE_DB_ARCHIVED_ACCESS_READER_ENTITY_MANAGER_NAME'),
];
