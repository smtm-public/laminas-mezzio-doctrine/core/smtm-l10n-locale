<?php

declare(strict_types=1);

namespace Smtm\L10n\Locale;

return [
    'em' => 'default',

    'table_storage' => [
        'table_name' => 'doctrine_migrations_smtm_l10n_locale',
    ],

    'migrations_paths' => [
        'Smtm\L10n\Locale\Migration' => __DIR__ . '/../data/db/migrations',
    ],
];
