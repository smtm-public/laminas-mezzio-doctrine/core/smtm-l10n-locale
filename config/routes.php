<?php

declare(strict_types=1);

namespace Smtm\L10n\Locale;

use Smtm\Base\Infrastructure\Helper\EnvHelper;

if (file_exists(__DIR__ . '/../../../../.env.smtm.smtm-l10n-locale')) {
    $dotenv = \Dotenv\Dotenv::createMutable(
        __DIR__ . '/../../../../',
        '.env.smtm.smtm-l10n-locale'
    );
    $dotenv->load();
}

$exposedRoutes = json_decode(
    EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_L10N_LOCALE_EXPOSED_ROUTES', '[]'),
    true
);
$exposedRoutes = array_combine($exposedRoutes, $exposedRoutes);

$routes = [
    'smtm.l10n.locale.locale-posix.read' => [
        'path' => '/l10n/locale/locale/{uuid}',
        'method' => 'get',
        'middleware' => Context\LocalePosix\Http\Handler\ReadHandler::class,
        'options' => [
            'authentication' => null,
            'authorization' => null,
            'requestValidatingInputFilterCollectionName' => null,
        ],
    ],
    'smtm.l10n.locale.locale-posix.index' => [
        'path' => '/l10n/locale/locale',
        'method' => 'get',
        'middleware' => Context\LocalePosix\Http\Handler\IndexHandler::class,
        'options' => [
            'authentication' => null,
            'authorization' => null,
            'requestValidatingInputFilterCollectionName' => null,
        ],
    ],
];

return array_intersect_key($routes, $exposedRoutes);
