<?php

declare(strict_types=1);

namespace Smtm\L10n\Locale\Migration;

use Smtm\Base\Infrastructure\Doctrine\Migration\UuidStringColumnAndUniqueIndexMigrationTrait;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Types;
use Doctrine\Migrations\AbstractMigration;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Version20201202120000 extends AbstractMigration
{

    use UuidStringColumnAndUniqueIndexMigrationTrait;

    public function up(Schema $schema): void
    {
        $this->createI18nLocalePosixTable($schema);
    }

    public function createI18nLocalePosixTable(Schema $schema): void
    {
        $l10nLocalePosixTable = $schema->createTable('l10n_locale_posix');
        $l10nLocalePosixTable->addColumn('id', Types::INTEGER);
        $l10nLocalePosixTable->setPrimaryKey(['id']);
        $this->addUuidStringColumnAndUniqueIndex($l10nLocalePosixTable);
        $l10nLocalePosixTable->addColumn(
            'r_name', Types::STRING,
            ['length' => 20, 'notNull' => false]
        );
        $l10nLocalePosixTable->addUniqueIndex(
            ['r_name'],
            'idx_unq_' . $l10nLocalePosixTable->getName() . '_r_name'
        );
        $l10nLocalePosixTable->addColumn(
            'code_set', Types::STRING,
            ['length' => 20, 'notNull' => false]
        );
        $l10nLocalePosixTable->addColumn(
            'description', Types::STRING,
            ['length' => 255, 'notNull' => false]
        );
        $l10nLocalePosixTable->addColumn(
            'recommended',
            Types::SMALLINT,
            ['notNull' => true, 'default' => 0]
        );
    }

    public function down(Schema $schema): void
    {
        $schema->dropTable('i18n_territorial_division');
    }
}
