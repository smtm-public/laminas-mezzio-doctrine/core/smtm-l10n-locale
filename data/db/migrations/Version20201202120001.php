<?php

declare(strict_types=1);

namespace Smtm\L10n\Locale\Migration;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Version20201202120001 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->populateL10nUsStateTable($schema);
    }

    public function populateL10nUsStateTable(Schema $schema): void
    {
        $this->connection->insert('l10n_locale_posix', [
            'id' => 1,
            'uuid' => '1b79be04-a36c-0079-532b-501fd284c574',
            'r_name' => 'C',
            'code_set' => 'US-ASCII',
            'description' => '',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 2,
            'uuid' => '51eb256b-4bfe-a847-f7eb-b3190e488d8f',
            'r_name' => 'POSIX',
            'code_set' => 'US-ASCII',
            'description' => '',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 3,
            'uuid' => '885d59ae-7466-92ee-d2fe-2907b5af13a0',
            'r_name' => 'af_ZA.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Afrikaans, South Africa',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 4,
            'uuid' => '0922a882-f383-1755-610a-c388c22df805',
            'r_name' => 'ar_AE.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Arabic, United Arab Emirates',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 5,
            'uuid' => '3eaf98fd-2ffd-5357-6d0f-64b1c16e2b13',
            'r_name' => 'ar_BH.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Arabic, Bahrain',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 6,
            'uuid' => 'e72260fc-69d1-b1c8-8aaa-41eb9bdf1e63',
            'r_name' => 'ar_DZ.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Arabic, Algeria',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 7,
            'uuid' => 'edab7edd-d6b0-cd65-e176-07367abf87c7',
            'r_name' => 'ar_EG.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Arabic, Egypt',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 8,
            'uuid' => '9cac6e44-8c43-2d89-71bc-448007717479',
            'r_name' => 'ar_IQ.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Arabic, Iraq',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 9,
            'uuid' => '8d4265e3-87cd-775c-df65-8437f7ee45a8',
            'r_name' => 'ar_JO.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Arabic, Jordan',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 10,
            'uuid' => '2864a320-59b9-93d0-b919-b062d8edf226',
            'r_name' => 'ar_KW.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Arabic, Kuwait',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 11,
            'uuid' => '5925319c-07a7-f99a-27b8-70826d6ede31',
            'r_name' => 'ar_LY.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Arabic, Libya',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 12,
            'uuid' => '642a3963-f5a6-2e55-c689-ccc407efefd3',
            'r_name' => 'ar_MA.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Arabic, Morocco',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 13,
            'uuid' => 'aaafc234-8c2d-9c99-20fb-00afec41652a',
            'r_name' => 'ar_OM.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Arabic, Oman',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 14,
            'uuid' => 'd227420e-e244-cac0-dde2-839f8979bc5d',
            'r_name' => 'ar_QA.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Arabic, Qatar',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 15,
            'uuid' => 'df6a3943-6329-8dd2-0330-64080e9a8296',
            'r_name' => 'ar_SA.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Arabic, Saudi Arabia',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 16,
            'uuid' => 'ce17ab85-072f-caf4-c548-f6d1fb0f8212',
            'r_name' => 'ar_TN.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Arabic, Tunisia',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 17,
            'uuid' => '259d7869-c0b9-a2db-31fa-f4db7d1a959c',
            'r_name' => 'ar_YE.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Arabic, Yemen',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 18,
            'uuid' => '7d5f1412-abde-8c83-f4b6-b9674a408d27',
            'r_name' => 'as_IN.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Assamese, India',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 19,
            'uuid' => '4c05f5ca-b724-506c-3622-e728fca7d07e',
            'r_name' => 'az_AZ.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Azerbaijani, Azerbaijan',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 20,
            'uuid' => '0c75232f-5a75-339f-1af0-ed184f10e5db',
            'r_name' => 'be_BY.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Belarusian, Belarus',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 21,
            'uuid' => '2c5b2f94-5f3f-2dbf-7d3c-7a338a6c2ae6',
            'r_name' => 'bg_BG.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Bulgarian, Bulgaria',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 22,
            'uuid' => '9566a639-06f8-6a45-2feb-f3444f0a803a',
            'r_name' => 'bn_IN.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Bengali, India',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 23,
            'uuid' => '693724a9-327a-60f9-df30-7180ee72986c',
            'r_name' => 'bs_BA.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Bosnian, Bosnia and Herzegovina',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 24,
            'uuid' => '4e29a49a-96f6-044b-7eaa-8ec8494a8437',
            'r_name' => 'ca_ES.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Catalan, Spain',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 25,
            'uuid' => 'b3cc6083-5d1e-0a14-d481-1026fb61741c',
            'r_name' => 'cs_CZ.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Czech, Czech Republic',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 26,
            'uuid' => 'e795bb25-4fd9-4de4-9677-78a2354bca90',
            'r_name' => 'da_DK.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Danish, Denmark',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 27,
            'uuid' => '9507a409-426b-1bba-436c-f8cced84b987',
            'r_name' => 'de_AT.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'German, Austria',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 28,
            'uuid' => '5f8d2f61-ff3c-6b37-6234-c630f21fbf4d',
            'r_name' => 'de_BE.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'German, Belgium',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 29,
            'uuid' => '08198ba2-1288-f2c8-1702-e2ba8395356b',
            'r_name' => 'de_CH.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'German, Switzerland',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 30,
            'uuid' => 'abb1aaf1-bb2c-541e-a7b8-5ca3d11e6fbe',
            'r_name' => 'de_DE.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'German, Germany',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 31,
            'uuid' => '9fd8ee8b-e5eb-3cc5-5a8c-a3e954817c3f',
            'r_name' => 'de_LI.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'German, Liechtenstein',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 32,
            'uuid' => '51dd9a80-83ef-5d21-2422-003679fe8126',
            'r_name' => 'de_LU.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'German, Luxembourg',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 33,
            'uuid' => '5eb1383a-bd59-9456-74be-b42693ffdcf3',
            'r_name' => 'el_CY.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Greek, Cyprus',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 34,
            'uuid' => '0f963545-7fb0-48b6-c0b7-d69248dfad3f',
            'r_name' => 'el_GR.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Greek, Greece',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 35,
            'uuid' => '6aadac57-269b-f6b1-ab91-3fe45e43c64b',
            'r_name' => 'en_AU.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'English, Australia',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 36,
            'uuid' => 'b05d0924-ff0d-d28a-9386-fd3c6c1ce73f',
            'r_name' => 'en_BW.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'English, Botswana',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 37,
            'uuid' => '24cb1aa7-9820-2dee-599a-e78989f08458',
            'r_name' => 'en_CA.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'English, Canada',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 38,
            'uuid' => 'da3bb782-8813-81ed-dc36-e6539420a53e',
            'r_name' => 'en_GB.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'English, United Kingdom',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 39,
            'uuid' => '5d4c5550-d37b-8215-dab7-2e422bfae303',
            'r_name' => 'en_HK.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'English, Hong Kong SAR China',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 40,
            'uuid' => '8e92fbb5-0ea9-7c0f-36ab-00d0f1ecbc23',
            'r_name' => 'en_IE.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'English, Ireland',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 41,
            'uuid' => '2874e631-1d39-6932-8682-6e042eb8ae54',
            'r_name' => 'en_IN.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'English, India',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 42,
            'uuid' => '66d1f580-7338-e09c-bbe9-7762d7dd853c',
            'r_name' => 'en_MT.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'English, Malta',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 43,
            'uuid' => 'a158bed4-83cc-19ab-edca-a760e7f61dfe',
            'r_name' => 'en_NZ.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'English, New Zealand',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 44,
            'uuid' => '966cf594-d10a-be38-5ee3-7ed351788203',
            'r_name' => 'en_PH.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'English, Philippines',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 45,
            'uuid' => 'e4560505-a276-56bf-a75a-b32614e03837',
            'r_name' => 'en_SG.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'English, Singapore',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 46,
            'uuid' => '86b7f486-9a67-8646-b36a-18d8441b429d',
            'r_name' => 'en_US.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'English, U.S.A.',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 47,
            'uuid' => 'edb1dc5e-b3e8-955e-2c24-0b3a40b324eb',
            'r_name' => 'en_ZW.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'English, Zimbabwe',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 48,
            'uuid' => '3bfa2358-3d2f-12ca-d7ac-991ac441a50b',
            'r_name' => 'es_AR.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Spanish, Argentina',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 49,
            'uuid' => '1da02060-c09a-6ef0-4e4a-3277c839b7f7',
            'r_name' => 'es_BO.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Spanish, Bolivia',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 50,
            'uuid' => 'dcc1e3ba-b71a-d6c5-2792-d0eb06f0bb1d',
            'r_name' => 'es_CL.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Spanish, Chile',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 51,
            'uuid' => '22698964-a1ba-def5-f95b-1c8075d07b5f',
            'r_name' => 'es_CO.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Spanish, Colombia',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 52,
            'uuid' => '2192533e-718a-ede1-d18d-d93cac0ac7b9',
            'r_name' => 'es_CR.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Spanish, Costa Rica',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 53,
            'uuid' => 'fc972696-1439-4f1b-8bb4-642df0afc2b2',
            'r_name' => 'es_DO.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Spanish, Dominican Republic',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 54,
            'uuid' => 'ac530730-ec73-647b-ec03-78525287f351',
            'r_name' => 'es_EC.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Spanish, Ecuador',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 55,
            'uuid' => '33ec9259-4e60-2bcc-61f5-aef5079ed0b6',
            'r_name' => 'es_ES.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Spanish, Spain',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 56,
            'uuid' => '0180f048-c971-6a8e-b7f2-b0f4eb0ca743',
            'r_name' => 'es_GT.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Spanish, Guatemala',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 57,
            'uuid' => '3c8f70c9-fa0d-bd60-ff73-e2f3809b50e6',
            'r_name' => 'es_HN.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Spanish, Honduras',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 58,
            'uuid' => 'baea4443-a891-90d7-28c6-173009b66d91',
            'r_name' => 'es_MX.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Spanish, Mexico',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 59,
            'uuid' => 'f615a302-8c20-a5eb-d1ba-aca54fc1cf80',
            'r_name' => 'es_NI.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Spanish, Nicaragua',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 60,
            'uuid' => '919aca72-6351-3024-29c4-ca026730d8a5',
            'r_name' => 'es_PA.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Spanish, Panama',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 61,
            'uuid' => '096e08fe-a7d9-a72f-8376-c7caa4e7749b',
            'r_name' => 'es_PE.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Spanish, Peru',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 62,
            'uuid' => 'c1ab47bd-1df9-c7b3-4023-8671ea750e27',
            'r_name' => 'es_PR.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Spanish, Puerto Rico',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 63,
            'uuid' => '4f464e67-81cf-3e93-fc97-7cb45fb7b911',
            'r_name' => 'es_PY.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Spanish, Paraguay',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 64,
            'uuid' => 'c1b99846-c3b3-7bd1-ac58-0d6c940500ce',
            'r_name' => 'es_SV.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Spanish, ElSalvador',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 65,
            'uuid' => '1e722152-3321-ce3d-4916-1c2a55eb0f68',
            'r_name' => 'es_US.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Spanish, U.S.A.',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 66,
            'uuid' => '3a2f04cb-f11b-b484-7025-7d3b78c0b7c8',
            'r_name' => 'es_UY.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Spanish, Uruguay',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 67,
            'uuid' => '269b6a53-8a37-0b77-8c41-19ad58e38021',
            'r_name' => 'es_VE.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Spanish, Venezuela',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 68,
            'uuid' => '5d372bc0-1d65-cc55-6a2e-11192c9e5743',
            'r_name' => 'et_EE.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Estonian, Estonia',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 69,
            'uuid' => '740b5405-40b4-3a6a-3324-b1441370d21e',
            'r_name' => 'fi_FI.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Finnish, Finland',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 70,
            'uuid' => 'cab2c308-4ff7-f18c-88b7-d37566b0553a',
            'r_name' => 'fr_BE.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'French, Belgium',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 71,
            'uuid' => 'a4080a84-ca56-3c83-8f82-567bc9e2aa58',
            'r_name' => 'fr_CA.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'French, Canada',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 72,
            'uuid' => 'd0963d3e-aefe-6694-c8a9-424143111778',
            'r_name' => 'fr_CH.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'French, Switzerland',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 73,
            'uuid' => 'c45e72fe-75a2-7e18-e0cc-468b16a00e6c',
            'r_name' => 'fr_FR.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'French, France',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 74,
            'uuid' => '12781e6f-1be6-6943-7b55-f08b18d1c8a1',
            'r_name' => 'fr_LU.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'French, Luxembourg',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 75,
            'uuid' => '180c0a3b-cff7-1371-a9a2-996e4dfe9740',
            'r_name' => 'gu_IN.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Gujarati, India',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 76,
            'uuid' => '1477daeb-1878-de58-926c-614edb07b200',
            'r_name' => 'he_IL.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Hebrew, Israel',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 77,
            'uuid' => '7faf58c0-c0b7-315b-caea-ec2768c48888',
            'r_name' => 'hi_IN.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Hindi, India',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 78,
            'uuid' => '51c4df99-17b1-03ab-548a-b483411d7756',
            'r_name' => 'hr_HR.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Croatian, Croatia',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 79,
            'uuid' => 'e835d3af-a13b-4b9a-1679-b854d9e2d7a0',
            'r_name' => 'hu_HU.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Hungarian, Hungary',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 80,
            'uuid' => '99e575d0-a0b3-ad86-4ef9-ba6f4e549a14',
            'r_name' => 'hy_AM.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Armenian, Armenia',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 81,
            'uuid' => '4075569c-6f4f-dbae-b8c0-fe4c6a8f77ca',
            'r_name' => 'id_ID.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Indonesian, Indonesia',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 82,
            'uuid' => '147e6fd2-2cc4-9142-dfd7-e25ab3d927e2',
            'r_name' => 'is_IS.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Icelandic, Iceland',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 83,
            'uuid' => 'd823fc8b-059d-9b8f-903b-02651a99c31f',
            'r_name' => 'it_CH.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Italian, Switzerland',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 84,
            'uuid' => 'af01b6fe-f161-475b-816f-b908157455f9',
            'r_name' => 'it_IT.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Italian, Italy',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 85,
            'uuid' => '7baa28d7-fa76-6dde-6d19-334429cb86f5',
            'r_name' => 'ja_JP.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Japanese, Japan',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 86,
            'uuid' => '52074d0d-4055-257a-ab2d-27b13e5c8cfc',
            'r_name' => 'ka_GE.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Georgian, Georgia',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 87,
            'uuid' => '6200f874-8edc-c468-6f3d-f83dd9ce3cd0',
            'r_name' => 'kk_KZ.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Kazakh, Kazakhstan',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 88,
            'uuid' => 'a9ca14c1-95c0-59d5-9e46-299d65935595',
            'r_name' => 'kn_IN.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Kannada, India',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 89,
            'uuid' => 'd836f3c6-3040-b985-4fbf-d935618b682f',
            'r_name' => 'ko_KR.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Korean, Korea',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 90,
            'uuid' => 'fbac9ef5-5789-a9cc-de92-3ec57b3b65a5',
            'r_name' => 'ks_IN.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Kashmiri, India',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 91,
            'uuid' => 'd61db001-37c1-37a0-8075-b4fef2ce870f',
            'r_name' => 'ku_TR.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Kurdish, Turkey',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 92,
            'uuid' => 'f9566b48-9bfe-3460-c876-ec2e120eab09',
            'r_name' => 'ku_TR.UTF-8@sorani',
            'code_set' => 'UTF-8',
            'description' => 'Kurdish (Sorani), Turkey',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 93,
            'uuid' => '54265dd2-6efb-d0bf-34c0-29625c2572a4',
            'r_name' => 'ky_KG.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Kirghiz, Kyrgyzstan',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 94,
            'uuid' => '02a5343c-3598-e9f5-6b0f-8cce9d5b2497',
            'r_name' => 'lt_LT.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Lithuanian, Lithuania',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 95,
            'uuid' => 'da7d99e2-f427-9a86-6f34-27c5e71980d6',
            'r_name' => 'lv_LV.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Latvian, Latvia',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 96,
            'uuid' => 'a81d5140-2494-883e-f86c-2fe37a29fdd9',
            'r_name' => 'mk_MK.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Macedonian, Macedonia',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 97,
            'uuid' => 'bc50d5c5-853b-940e-b777-ca8980b6491e',
            'r_name' => 'ml_IN.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Malayalam, India',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 98,
            'uuid' => 'b0cf35ab-aa4e-7b00-cffb-fb62efce7f7d',
            'r_name' => 'mr_IN.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Marathi, India',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 99,
            'uuid' => 'f364ac8f-1e00-ace4-6270-9b3375ed8452',
            'r_name' => 'ms_MY.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Malay, Malaysia',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 100,
            'uuid' => 'ef23207f-34cb-90c4-ce45-640e57225e82',
            'r_name' => 'mt_MT.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Maltese, Malta',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 101,
            'uuid' => '203f9880-b12d-7c4c-00ad-cf17dec6996b',
            'r_name' => 'nb_NO.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Bokmal, Norway',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 102,
            'uuid' => '32f5be8e-fc02-0a72-4ba0-97d788720a04',
            'r_name' => 'nl_BE.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Dutch, Belgium',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 103,
            'uuid' => 'a78d8575-6834-7b64-583f-16a33dba179f',
            'r_name' => 'nl_NL.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Dutch, Netherlands',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 104,
            'uuid' => '43800ea9-c866-25d5-81f5-2ce67769f231',
            'r_name' => 'nn_NO.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Nynorsk, Norway',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 105,
            'uuid' => '2cea6225-5434-bd90-5eae-9751aa9237f2',
            'r_name' => 'or_IN.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Oriya, India',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 106,
            'uuid' => '4e3aa2ff-54a6-8a61-2fcc-0e0c28176603',
            'r_name' => 'pa_IN.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Punjabi, India',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 107,
            'uuid' => 'f2927f75-9a2a-0577-fb9d-4a092c4c7121',
            'r_name' => 'pl_PL.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Polish, Poland',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 108,
            'uuid' => '84acee73-acf0-c5ac-0c8d-f91678c878c0',
            'r_name' => 'pt_BR.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Portuguese, Brazil',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 109,
            'uuid' => '17adad9f-1fdc-558d-dc39-922f23d49ebd',
            'r_name' => 'pt_PT.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Portuguese, Portugal',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 110,
            'uuid' => 'cd4bca21-40d9-ebdb-432a-b73a2d049dc8',
            'r_name' => 'ro_RO.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Romanian, Romania',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 111,
            'uuid' => '60d9582c-424d-b013-0a06-15ecf5a9acba',
            'r_name' => 'ru_RU.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Russian, Russia',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 112,
            'uuid' => 'f5309045-4cb8-4751-1e94-0fb7001cf5be',
            'r_name' => 'ru_UA.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Russian, Ukraine',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 113,
            'uuid' => 'da6c7568-d885-a161-50ba-eec6780f4939',
            'r_name' => 'sa_IN.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Sanskrit, India',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 114,
            'uuid' => '6798b1be-66c5-ba18-2d43-d9dae930c177',
            'r_name' => 'sk_SK.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Slovak, Slovakia',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 115,
            'uuid' => 'ce04cda4-abbe-598a-a8d9-ba14e8ad6ab3',
            'r_name' => 'sl_SI.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Slovenian, Slovenia',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 116,
            'uuid' => '23c9c6e0-f5ad-816a-6d53-959eaed34574',
            'r_name' => 'sq_AL.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Albanian, Albania',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 117,
            'uuid' => 'bc1de349-9137-89d1-bfe1-478378758bfc',
            'r_name' => 'sr_ME.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Serbian, Montenegro',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 118,
            'uuid' => 'd0297ba2-6c5d-a97d-d116-b44d20368571',
            'r_name' => 'sr_ME.UTF-8@latin',
            'code_set' => 'UTF-8',
            'description' => 'Serbian, Montenegro (Latin)',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 119,
            'uuid' => '9fd60f4d-e404-0788-e5ca-e3687ea41436',
            'r_name' => 'sr_RS.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Serbian, Serbia',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 120,
            'uuid' => '6a26e8a2-2766-5920-ae9c-a1829af5e0a4',
            'r_name' => 'sr_RS.UTF-8@latin',
            'code_set' => 'UTF-8',
            'description' => 'Serbian, Serbia (Latin)',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 121,
            'uuid' => 'e3f3df46-1f6c-d698-099a-f60f7b3c2524',
            'r_name' => 'sv_SE.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Swedish, Sweden',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 122,
            'uuid' => '18b09764-b0d7-31ae-222c-3109444cb785',
            'r_name' => 'ta_IN.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Tamil, India',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 123,
            'uuid' => 'b8cd87e1-7d64-3a4d-1154-a6eccda85889',
            'r_name' => 'te_IN.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Telugu, India',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 124,
            'uuid' => '3080678a-15bb-b2d6-316f-6560fe24287b',
            'r_name' => 'th_TH.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Thai, Thailand',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 125,
            'uuid' => 'a3c96ee4-ca2c-1ed3-b077-5f067532d8af',
            'r_name' => 'tr_TR.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Turkish, Turkey',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 126,
            'uuid' => '1c786eb1-5acd-2673-31a4-dbdc3178c660',
            'r_name' => 'uk_UA.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Ukrainian, Ukraine',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 127,
            'uuid' => 'ef0fc0fa-c2a0-15f4-21d4-e33e523ab48a',
            'r_name' => 'vi_VN.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Vietnamese, Vietnam',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 128,
            'uuid' => 'fd12d8eb-30f5-e212-f67b-9938ed415115',
            'r_name' => 'zh_CN.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Simplified Chinese, China',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 129,
            'uuid' => '987a152f-9187-da22-cccc-55aa5389f177',
            'r_name' => 'zh_HK.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Traditional Chinese, Hong Kong SARChina',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 130,
            'uuid' => '4b2f5aec-2bb5-f165-99c2-45c5be5ff3ae',
            'r_name' => 'zh_SG.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Chinese, Singapore',
            'recommended' => 1,
        ]);
        $this->connection->insert('l10n_locale_posix', [
            'id' => 131,
            'uuid' => 'caf388c1-98f4-5eb8-144d-d4d09a85a481',
            'r_name' => 'zh_TW.UTF-8',
            'code_set' => 'UTF-8',
            'description' => 'Traditional Chinese, Taiwan',
            'recommended' => 1,
        ]);
    }

    public function down(Schema $schema): void
    {

    }
}
