<?php

declare(strict_types=1);

namespace Smtm\L10n\Locale\Command;

use Smtm\Base\Infrastructure\Helper\OAuth2Helper;
use Smtm\Base\Infrastructure\Service\Log\LoggerAwareInterface;
use Smtm\Base\Infrastructure\Service\Log\LoggerAwareTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 *
 * Example:
 * vendor/bin/symfony-console l10n-locale:generate-migration-code-for-table-locale-posix -vvv
 */
class GenerateMigrationCodeForTableLocalePosix extends Command implements LoggerAwareInterface
{

    use LoggerAwareTrait;

    // https://docs.oracle.com/cd/E23824_01/html/E26033/glset.html
    protected string $htmlTableCode = <<< EOT
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>C</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>US-ASCII</tt> </div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>C</tt> , <tt>POSIX</tt></div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>POSIX</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>US-ASCII</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>C</tt>, <tt>POSIX</tt></div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>af_ZA.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Afrikaans, South Africa</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>ar_AE.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Arabic, United Arab Emirates</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>ar_BH.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Arabic, Bahrain</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>ar_DZ.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Arabic,
    Algeria</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>ar_EG.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Arabic, Egypt</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>ar_IQ.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Arabic, Iraq</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>ar_JO.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Arabic, Jordan</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>ar_KW.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Arabic, Kuwait </div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>ar_LY.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Arabic, Libya</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>ar_MA.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Arabic, Morocco </div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>ar_OM.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Arabic, Oman</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>ar_QA.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Arabic, Qatar </div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>ar_SA.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Arabic,
    Saudi Arabia</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>ar_TN.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Arabic, Tunisia</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>ar_YE.UTF-8</tt> </div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Arabic, Yemen</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>as_IN.UTF-8</tt> </div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Assamese, India</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>az_AZ.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Azerbaijani, Azerbaijan </div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>be_BY.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Belarusian, Belarus</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>bg_BG.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Bulgarian, Bulgaria </div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>bn_IN.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Bengali,
    India</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>bs_BA.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Bosnian, Bosnia and Herzegovina</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>ca_ES.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Catalan, Spain</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>cs_CZ.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Czech, Czech Republic</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>da_DK.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Danish, Denmark</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>de_AT.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">German, Austria</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>de_BE.UTF-8</tt> </div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">German, Belgium</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>de_CH.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">German, Switzerland</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>de_DE.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">German,
    Germany</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>de_LI.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">German, Liechtenstein </div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>de_LU.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">German, Luxembourg</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>el_CY.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Greek, Cyprus </div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>el_GR.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Greek, Greece</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>en_AU.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">English, Australia</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>en_BW.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">English, Botswana</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>en_CA.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">English, Canada</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>en_GB.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">English, United Kingdom</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>en_HK.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">English,
    Hong Kong SAR China</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>en_IE.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">English, Ireland</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>en_IN.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">English, India </div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>en_MT.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">English, Malta</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>en_NZ.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">English, New Zealand</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>en_PH.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">English, Philippines</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>en_SG.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">English, Singapore</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>en_US.UTF-8</tt>
    </div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">English, U.S.A.</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>en_ZW.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">English, Zimbabwe</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>es_AR.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Spanish, Argentina</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>es_BO.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Spanish, Bolivia</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>es_CL.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Spanish, Chile</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>es_CO.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Spanish, Colombia</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>es_CR.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Spanish, Costa Rica </div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>es_DO.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Spanish, Dominican Republic</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>es_EC.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Spanish,
    Ecuador</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>es_ES.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Spanish, Spain</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>es_GT.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Spanish, Guatemala</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>es_HN.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Spanish, Honduras</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>es_MX.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Spanish, Mexico</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>es_NI.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Spanish, Nicaragua</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>es_PA.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Spanish, Panama</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>es_PE.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Spanish, Peru</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>es_PR.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Spanish, Puerto Rico</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>es_PY.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Spanish, Paraguay</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>es_SV.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Spanish, El
    Salvador</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>es_US.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Spanish, U.S.A.</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>es_UY.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Spanish, Uruguay</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>es_VE.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Spanish, Venezuela</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>et_EE.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Estonian, Estonia</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>fi_FI.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Finnish, Finland</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>fr_BE.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">French, Belgium</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>fr_CA.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">French, Canada</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>fr_CH.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"> French, Switzerland</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>fr_FR.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">French, France</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>fr_LU.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">French, Luxembourg</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>gu_IN.UTF-8 </tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Gujarati,
    India</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>he_IL.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Hebrew, Israel</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>hi_IN.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Hindi, India</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>hr_HR.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Croatian, Croatia</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>hu_HU.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Hungarian, Hungary</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>hy_AM.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Armenian, Armenia</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>id_ID.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Indonesian, Indonesia</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>is_IS.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Icelandic, Iceland</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>it_CH.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Italian, Switzerland</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>it_IT.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Italian, Italy</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>ja_JP.UTF-8</tt> </div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Japanese, Japan</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>ka_GE.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Georgian,
    Georgia</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>kk_KZ.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Kazakh, Kazakhstan</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>kn_IN.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Kannada, India</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>ko_KR.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Korean, Korea</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>ks_IN.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Kashmiri, India</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>ku_TR.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Kurdish, Turkey</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>ku_TR.UTF-8@sorani</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Kurdish (Sorani), Turkey</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>ky_KG.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Kirghiz, Kyrgyzstan</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>lt_LT.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Lithuanian, Lithuania</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>lv_LV.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Latvian, Latvia</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>mk_MK.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Macedonian, Macedonia</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>ml_IN.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Malayalam,
    India</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>mr_IN.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Marathi, India</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>ms_MY.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Malay, Malaysia </div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>mt_MT.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Maltese, Malta</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>nb_NO.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Bokmal, Norway</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>nl_BE.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Dutch, Belgium</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>nl_NL.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Dutch, Netherlands</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>nn_NO.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Nynorsk, Norway </div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>or_IN.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Oriya, India</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>pa_IN.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Punjabi, India</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>pl_PL.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Polish,
    Poland</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>pt_BR.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Portuguese, Brazil</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>pt_PT.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Portuguese, Portugal</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>ro_RO.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Romanian, Romania</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>ru_RU.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Russian, Russia</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>ru_UA.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Russian, Ukraine </div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>sa_IN.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Sanskrit, India</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>sk_SK.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Slovak, Slovakia </div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>sl_SI.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Slovenian, Slovenia</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>sq_AL.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Albanian, Albania</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>sr_ME.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Serbian,
    Montenegro </div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>sr_ME.UTF-8@latin</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Serbian, Montenegro (Latin) </div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>sr_RS.UTF-8</tt> </div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Serbian, Serbia</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>sr_RS.UTF-8@latin</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Serbian, Serbia (Latin)</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>sv_SE.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Swedish, Sweden</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>ta_IN.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Tamil, India</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>te_IN.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Telugu, India</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>th_TH.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Thai,
    Thailand </div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>tr_TR.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Turkish, Turkey</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>uk_UA.UTF-8</tt> </div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Ukrainian, Ukraine</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>vi_VN.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Vietnamese, Vietnam</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>zh_CN.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Simplified Chinese, China</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>zh_HK.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Traditional Chinese, Hong Kong SAR
    China</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>zh_SG.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Chinese, Singapore</div></td>
    </tr>
    
    <tr class="yellow2">
    <td scope="row" align="left" valign="top"><div class="pad5x10"><tt>zh_TW.UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10"><tt>UTF-8</tt></div></td>
    <td align="left" valign="top"><div class="pad5x10">Traditional Chinese, Taiwan </div></td>
    </tr>
    EOT;

    protected function configure()
    {
        $this
            ->setName('l10n-locale:generate-migration-code-for-table-locale-posix')
            ->setDescription(
                'Smtm - L10n - Generate Migration Code for Table l10n_locale_posix'
            )
            // the full command description shown when running the command with
            // the "--help" option
            //->setHelp('Could write some useful text here')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $migrationCodeLines = $this->generateLocalePosixMigrationCode($this->htmlTableCode);
        $migrationCode = implode("\n", $migrationCodeLines);
        $this->logger->info("\n" . $migrationCode);

        return Command::SUCCESS;
    }

    protected function generateLocalePosixMigrationCode(string $htmlTableCode): array
    {
        $lines = preg_replace_callback(
            '%<tr.*?>\R*<td.*?<tt>((?:.|\R)*?)\R*</tt>(?:\R|\s)*?</div>\R*</td>\R*<td.*?<tt>((?:.|\R)*?)</tt.*?/td>\R*<td.*?<div.*?>((?:.|\R)*?)<.*?/td>\R*</tr>\R*%m',
            function (array $matches) {
                array_shift($matches);
                $matches = preg_replace('#\R+#', '', $matches);
                $matches = array_map('trim', $matches);
                $matches[2] = preg_replace('#,\s*#', ', ', $matches[2]);

                return implode(
                        '|',
                        $matches
                    ) . "\n";
            },
            $htmlTableCode
        );

        $localesMigrationCodeLines = preg_split('#\R#', $lines);
        $id = 0;
        $lines = [];

        foreach ($localesMigrationCodeLines as $localesMigrationCodeLine) {
            $tds = explode('|', $localesMigrationCodeLine);

            $uuid = bin2hex(random_bytes(4)) . '-' . bin2hex(random_bytes(2)) . '-' . bin2hex(
                    random_bytes(2)
                ) . '-' . bin2hex(random_bytes(2)) . '-' . bin2hex(random_bytes(6));

            $id++;

            $lines[] = <<< EOT
            \$this->connection->insert('l10n_locale_posix', [
                'id' => $id,
                'uuid' => '$uuid',
                'r_name' => '$tds[0]',
                'code_set' => '$tds[1]',
                'description' => '$tds[2]',
                'recommended' => 1,
            ]);
            EOT;
        }

        return $lines;
    }
}
