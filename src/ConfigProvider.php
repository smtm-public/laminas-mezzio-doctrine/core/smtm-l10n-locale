<?php

declare(strict_types=1);

namespace Smtm\L10n\Locale;

use JetBrains\PhpStorm\ArrayShape;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ConfigProvider
{
    #[ArrayShape([
        'dependencies' => 'array',
        'doctrine' => 'array',
        'routes' => 'array',
        'l10n' => 'array',
    ])] public function __invoke(): array
    {
        return [
            'dependencies' => include __DIR__ . '/../config/dependencies.php',
            'doctrine' => include __DIR__ . '/../config/doctrine.php',
            'routes' => include __DIR__ . '/../config/routes.php',
            'l10n' => [
                'locale' => include __DIR__ . '/../config/l10n-locale.php',
            ],
        ];
    }
}
