<?php

declare(strict_types=1);

namespace Smtm\L10n\Locale\Context\LocalePosix\Application\Extractor;

use Smtm\Base\Application\Extractor\AbstractDomainObjectExtractor;

class LocalePosixExtractor extends AbstractDomainObjectExtractor
{
    protected array $properties = [
        'uuid' => null,
        'name' => null,
        'officialStateName' => null,
        'sovereignty' => null,
        'codeIso3166Alpha2' => null,
        'codeIso3166Alpha3' => null,
        'codeIso3166Numeric' => null,
        'ccTld' => null,
    ];
}
