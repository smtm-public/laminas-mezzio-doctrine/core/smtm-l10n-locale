<?php

declare(strict_types=1);

namespace Smtm\L10n\Locale\Context\LocalePosix\Application\Hydrator;

use Smtm\Base\Application\Hydrator\DomainObjectHydrator;

class LocalePosixHydrator extends DomainObjectHydrator
{
    /** @inheritdoc  */
    protected array $mustHydrate = [
        'name' => 'You must specify a name for the LocalePosix.',
        'officialStateName' => 'You must specify an officialStateName for the LocalePosix.',
        'sovereignty' => 'You must specify sovereignty for the LocalePosix.',
        'codeIso3166Alpha2' => 'You must specify a codeIso3166Alpha2 for the LocalePosix.',
        'codeIso3166Alpha3' => 'You must specify a codeIso3166Alpha3 for the LocalePosix.',
        'codeIso3166Numeric' => 'You must specify a codeIso3166Numeric for the LocalePosix.',
        'ccTld' => 'You must specify a ccTld for the LocalePosix.',
    ];
}
