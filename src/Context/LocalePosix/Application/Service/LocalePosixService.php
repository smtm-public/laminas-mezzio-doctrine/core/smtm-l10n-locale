<?php

declare(strict_types=1);

namespace Smtm\L10n\Locale\Context\LocalePosix\Application\Service;

use Smtm\Base\Application\Service\ApplicationService\AbstractDbService;
use Smtm\Base\Application\Service\ApplicationService\DbConfigurableEntityManagerNameServiceInterface;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbServiceInterface;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbServiceTrait;
use Smtm\Base\ConfigAwareTrait;
use Smtm\L10n\Locale\Context\LocalePosix\Application\Hydrator\LocalePosixHydrator;
use Smtm\L10n\Locale\Context\LocalePosix\Domain\LocalePosix;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class LocalePosixService extends AbstractDbService implements
    UuidAwareEntityDbServiceInterface,
    DbConfigurableEntityManagerNameServiceInterface
{

    use UuidAwareEntityDbServiceTrait, ConfigAwareTrait;

    protected ?string $domainObjectName = LocalePosix::class;
    protected ?string $hydratorName = LocalePosixHydrator::class;
}
