<?php

declare(strict_types=1);

namespace Smtm\L10n\Locale\Context\LocalePosix\Domain;

use Smtm\Base\Domain\AbstractUuidAwareEntity;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class LocalePosix extends AbstractUuidAwareEntity
{
    protected string $name;
    protected string $codeSet;
    protected string $description;
    protected bool $recommended;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getCodeSet(): string
    {
        return $this->codeSet;
    }

    public function setCodeSet(string $codeSet): static
    {
        $this->codeSet = $codeSet;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function isRecommended(): bool
    {
        return $this->recommended;
    }

    public function setRecommended(bool $recommended): static
    {
        $this->recommended = $recommended;

        return $this;
    }
}
