<?php

declare(strict_types=1);

namespace Smtm\L10n\Locale\Context\LocalePosix\Http\Handler;

use Smtm\Base\Http\Handler\DbServiceEntity\UuidAware\AbstractReadHandler;
use Smtm\L10n\Locale\Context\LocalePosix\Application\Extractor\LocalePosixExtractor;
use Smtm\L10n\Locale\Context\LocalePosix\Application\Service\LocalePosixService;
use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ReadHandler extends AbstractReadHandler
{
    public ?string $applicationServiceName = LocalePosixService::class;
    public ?string $domainObjectExtractorName = LocalePosixExtractor::class;

    /**
     * @SWG\Post(path="/experimental/sso/sso/token",
     *   tags={"Token"},
     *   summary="Creates an SSO Token from auth code and return it back for further authentication",
     *   description="Creates access token by given authCode OR refreshToken",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *   @SWG\Response(
     *     response=201,
     *     description="Successfully saved data",
     *     @SWG\Schema(
     *       @SWG\Property(
     *              property="accessToken",
     *              type="string"
     *       ),
     *       @SWG\Property(
     *              property="refreshToken",
     *              type="string"
     *       ),
     *     )
     *   ),
     *   @SWG\Response(response=409, description="Invalid data"),
     *   security={{"Bearer":{}}}
     * )
     */
    public function handle(ServerRequestInterface $request): JsonResponse
    {
        return parent::handle($request);
    }
}
