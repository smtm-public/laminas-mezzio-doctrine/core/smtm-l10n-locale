<?php

declare(strict_types=1);

namespace Smtm\I18n\LocalePosix\Context\LocalePosix\Infrastructure\Repository;

use Smtm\Base\Infrastructure\Doctrine\Orm\AbstractRepository;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class LocalePosixRepository extends AbstractRepository implements
    LocalePosixRepositoryInterface
{

}
