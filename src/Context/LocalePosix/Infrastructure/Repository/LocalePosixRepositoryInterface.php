<?php

declare(strict_types=1);

namespace Smtm\I18n\LocalePosix\Context\LocalePosix\Infrastructure\Repository;

use Smtm\Base\Infrastructure\Repository\RepositoryInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface LocalePosixRepositoryInterface extends RepositoryInterface
{

}
