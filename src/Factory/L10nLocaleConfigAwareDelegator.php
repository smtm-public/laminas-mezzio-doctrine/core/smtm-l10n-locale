<?php

declare(strict_types=1);

namespace Smtm\L10n\Locale\Factory;

use Smtm\Base\Factory\ConfigAwareDelegator;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class L10nLocaleConfigAwareDelegator extends ConfigAwareDelegator
{
    protected array|string|null $configKey = ['l10n', 'locale'];
}
